/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import POJO.Student;

/**
 *
 * @author hp
 */
public class StudentModel {
    ArrayList<Student> std = new ArrayList();
    
    public void listAllStudents(){
        std.forEach((s) -> {
            System.out.println(s);
        });
    }
    boolean isAdded = false;
    public boolean addStudent(int id, String name){
        std.add(new Student(id,name));
        isAdded = true;
        return isAdded;
                
    }
    
    public void initialStudents(){
        addStudent(1, "Dhruvi");
        addStudent(2, "Bhumil");
        addStudent(3, "Dhrumil");
        addStudent(4, "Tanuja");
        addStudent(5, "Jigar");
        addStudent(6, "Krinal");
        addStudent(7, "Joy");
        addStudent(8, "Nimi");
        addStudent(9, "Hari");
        addStudent(10, "Shree");
    }
    
    public boolean chkRegStudent(String name){
        boolean res= false;
        for(int i =0; i<= std.size() - 1; i++) {
            Student number = std.get(i);
            if(number.name == null ? name == null : number.name.equals(name)) {
                res = true;
            }
        }
        return res;
    }
}
