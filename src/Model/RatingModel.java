/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import POJO.Rating;
import java.util.ArrayList;

/**
 *
 * @author hp
 */
public class RatingModel {
    ArrayList<Rating> rt = new ArrayList();
    
    public void listAllRatings(){
        rt.forEach((r) -> {
            System.out.println(r);
        });
    }
    
    boolean isRated = false;
    public boolean addRating(int star, String value){
        rt.add(new Rating(star, value));
        isRated = true;
        return isRated;
    }
    
    public void initialRatings(){
        addRating(1, "Very dissatisfied");
        addRating(3, "Ok");
        addRating(4, "Satisfied");
        addRating(1, "Very dissatisfied");
        addRating(2, "Dissatisfied");
        addRating(5, "Very Satisfied");
        addRating(3, "Ok");
        addRating(2, "Dissatisfied");
        addRating(1, "Very dissatisfied");
        addRating(4, "Satisfied");
        addRating(3, "Ok");
        addRating(5, "Very Satisfied");
        addRating(2, "Dissatisfied");
        addRating(3, "Ok");
        addRating(2, "Dissatisfied");
        addRating(1, "Very dissatisfied");
        addRating(4, "Satisfied");
        addRating(2, "Dissatisfied");
        addRating(3, "Ok");
        addRating(5, "Very Satisfied");
    }
}
