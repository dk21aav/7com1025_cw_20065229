/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;

/**
 *
 * @author hp
 */
public class Student {
    private final int sID;
    public String name;
    
    public Student(int sID, String name){
        this.sID = sID;
        this.name = name;
    }
    
    @Override
    public String toString(){
        return this.sID+" "+this.name;
    }
}
