/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import POJO.Lesson;
import java.util.ArrayList;

/**
 *
 * @author hp
 */
public class LessonModel {
    ArrayList<Lesson> monthlylsnrpt = new ArrayList();
    
    boolean isRptadded = false;
    public boolean addLsnRpt(String exname, String avgrate){
        monthlylsnrpt.add(new Lesson(exname, avgrate));
        isRptadded = true;
        return isRptadded;
    }
    
    public void initialData(){
        addLsnRpt("Exercise Name","Average Rating(out of 5)");
        addLsnRpt("Yoga         ","    4");
        addLsnRpt("Zumba        ","  4.7");
        addLsnRpt("Aquacise     ","    3");
        addLsnRpt("Boxfit       ","  2.6");
    }    
    String monthName;
    public void getRecordforRpt(int month){
        
        switch(month){
            case 1:
                monthName = "JANUARY";
                break;
            case 2:
                monthName = "FEBRUARY";
                break;
            case 3:
                monthName = "MARCH";
                break;
            case 4:
                monthName = "APRIL";
                break;
            case 5:
                monthName = "MAY";
                break;
            case 6:
                monthName = "JUNE";
                break;
            case 7:
                monthName = "JULY";
                break;
            case 8:
                monthName = "AUGUST";
                break;
            case 9:
                monthName = "SEPTEMBER";
                break;
            case 10:
                monthName = "OCTOBER";
                break;
            case 11:
                monthName = "NOVEMBER";
                break;
            case 12:
                monthName = "DECEMBER";
                break;
                
            
        }
        System.out.println("*********MONTHLY LESSON REPORT*********");
        System.out.println("Month-"+monthName);
        initialData();
        monthlylsnrpt.forEach((s) -> {
            System.out.println(s);
        });
    }
}