/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import POJO.Rating;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author hp
 */
public class RatingModelTest {
    
    public RatingModelTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of listAllRatings method, of class RatingModel.
     */
    @Test
    public void testListAllRatings() {
        System.out.println("listAllRatings");
        RatingModel instance = new RatingModel();
        ArrayList<Rating> rt = new ArrayList();
        rt.add(new Rating(3, "OK"));
        instance.listAllRatings();
        int expRes = 1;
        int result = rt.size();
        assertEquals(expRes, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addRating method, of class RatingModel.
     */
    @Test
    public void testAddRating() {
        System.out.println("addRating");
        int star = 3;
        String value = "OK";
        RatingModel instance = new RatingModel();
        boolean expRes = true;
        boolean result = instance.addRating(star, value);
        assertEquals(expRes, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
