/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;

/**
 *
 * @author hp
 */
public class Rating {
    private final int ratingStar;
    private final String value;
    
    public Rating(int ratingStar, String value){
        this.ratingStar = ratingStar;
        this.value = value;
    }
    
    @Override
    public String toString(){
        return ""+ this.ratingStar+ " "+ this.value;
    }
}
