/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import POJO.Timetable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author hp
 */
public class TimetableModel {
    ArrayList<Timetable> tbl = new ArrayList();
    ArrayList<Timetable> tbl1 = new ArrayList();
    BookingHistoryModel bmdl = new BookingHistoryModel();
    PriceModel pmdl = new PriceModel();
    

    public void listTimetable(){
        tbl.forEach((t) -> {
            System.out.println(t);
        });
    }
    
    public void timetableByDay(String dayname){
        if("Saturday".equals(dayname)){
           tbl1.clear();
            for(int i =0; i<= tbl.size() -1; i++) {
                    Timetable number = tbl.get(i);
                    if(number.toString().contains("Saturday")) {
                        tbl1.add(number);
                    }
                }
            tbl1.forEach((t) -> {
                System.out.println(t);
            }); 
        }
        else if("Sunday".equals(dayname)){
            tbl1.clear();
            for(int i =1; i<= tbl.size() - 1; i++) {
                    Timetable number = tbl.get(i);
                    if(number.toString().contains("Sunday")) {
                        tbl1.add(number);
                    }
                }
            tbl1.forEach((t) -> {
                System.out.println(t);
            });
        }
    }
    
    public void timetableByExName(String exname){
        if(null != exname)switch (exname) {
            case "Yoga":
                tbl1.clear();
                for(int i =1; i<= tbl.size() - 1; i++) {
                    Timetable number = tbl.get(i);
                    if(number.toString().contains("Yoga")) {
                        tbl1.add(number);
                    }
                }
                tbl1.forEach((t) -> {
                    System.out.println(t);
                }); break;
            case "Zumba":
                tbl1.clear();
                for(int i =1; i<= tbl.size() - 1; i++) {
                    Timetable number = tbl.get(i);
                    if(number.toString().contains("Zumba")) {
                        tbl1.add(number);
                    }
                }
                tbl1.forEach((t) -> {
                    System.out.println(t);
                }); break;
            case "Aquacise":
                tbl1.clear();
                for(int i =1; i<= tbl.size() - 1; i++) {
                    Timetable number = tbl.get(i);
                    if(number.toString().contains("Aquacise")) {
                        tbl1.add(number);
                    }
                }
                tbl1.forEach((t) -> {
                    System.out.println(t);
                }); break;
            case "Boxfit":
                tbl1.clear();
                for(int i =1; i<= tbl.size() - 1; i++) {
                    Timetable number = tbl.get(i);
                    if(number.toString().contains("Boxfit")) {
                        tbl1.add(number);
                    }
                }
                tbl1.forEach((t) -> {
                    System.out.println(t);
                }); break;
            default:
                break;
        }
    }
    public void defineTimetable(int id, String day,String session,String exercise,String week, int count, String bookedStudent, String price) {
        tbl.add(new Timetable(id,day,session,exercise,week,count,bookedStudent,price));
    }
    int incrementBKN = 120;
    boolean isBooked = false;
    public boolean bookLesson(int lId, String sname){
        int totalStudent;
        String bookedStudent;
        
        String bookingId = "BKN" + incrementBKN;
        for(int i=0; i< tbl.size(); i++){
           if(tbl.get(i).id == lId){
               Timetable row = tbl.get(i);
                   totalStudent = tbl.get(i).count;
                   bookedStudent = tbl.get(i).bookedStudent;
                   if(!bookedStudent.contains(sname)){
                       if(totalStudent < 4){
                         row.count = totalStudent +1;
                         row.bookedStudent = bookedStudent+","+sname;
                         System.out.println("BOOKING SUCCESSFUL");
                         System.out.println("Your Booking ID is--"+bookingId);
                         bmdl.addBookingHistory(bookingId, sname, row.session, row.exercise, row.week, lId);
                         pmdl.calculateIncome(row.exercise);
                         incrementBKN++;
                         isBooked = true;
                       }
                       else{
                           System.err.println("No space available in current session.");
                       }
                   }
                   else
                   {
                       System.err.println("You are already registed with same session.");
                   }
                   
           }
        }
        return isBooked;
    }
    
    public void listBooking(){
        bmdl.listAllBookings();
    }
    public void monthlyChampionRpt(int month){
        pmdl.monthlyChampionExRpt(month);
    }
    
    public void cancelBooking(String bId){
        int lId;
        String sname;
        String finalval;
        finalval = bmdl.cancelBooking(bId);
        String[] finalvalarr = finalval.split(",");
        lId = Integer.parseInt(finalvalarr[0]);
        sname = finalvalarr[1];
        
        for(int i=0; i< tbl.size(); i++){
            if(tbl.get(i).id == lId){
                Timetable row = tbl.get(i);
                row.count = row.count -1;
                String[] bookStudent = row.bookedStudent.split(",");
                if(Arrays.asList(bookStudent).contains(sname)){
                    for(int j=0; j<bookStudent.length; j++){
                       if(bookStudent[j].equals(sname)){
                            List<String> list = new ArrayList<>(Arrays.asList(bookStudent));
                            list.remove(sname);
                            bookStudent = list.toArray(new String[0]);
                       } 
                    }
                    String newBookedStudent = Arrays.toString(bookStudent);
                    row.bookedStudent = newBookedStudent;
                }
            }
        }
        System.out.println("Your Booking has been cancelled.");
    }
    Scanner input = new Scanner(System.in);
    String sname;
    public void changeBooking(String bId){
        int lId;
        sname = bmdl.getStudentName(bId);
        int oldLId;
        oldLId= bmdl.getOldLid(bId);
        listTimetable();
        System.out.println("Select a new lesson ID which you want to book.");
        lId = input.nextInt();
        for(int i=0; i< tbl.size(); i++){
            if(tbl.get(i).id == oldLId){
                Timetable row = tbl.get(i);
                row.count = row.count -1;
                String[] bookStudent = row.bookedStudent.split(",");
                if(Arrays.asList(bookStudent).contains(sname)){
                    for(int j=0; j<bookStudent.length; j++){
                       if(bookStudent[j].equals(sname)){
                            List<String> list = new ArrayList<>(Arrays.asList(bookStudent));
                            list.remove(sname);
                            bookStudent = list.toArray(new String[0]);
                       } 
                    }
                    String newBookedStudent = Arrays.toString(bookStudent);
                    row.bookedStudent = newBookedStudent;
                }
            }
        }
        int totalStudent;
        String bookedStudent;
        
        String bookingId = bId;
        for(int i=0; i< tbl.size(); i++){
           if(tbl.get(i).id == lId){
               Timetable row = tbl.get(i);
                   totalStudent = tbl.get(i).count;
                   bookedStudent = tbl.get(i).bookedStudent;
                   if(!bookedStudent.contains(sname)){
                       if(totalStudent < 4){
                         row.count = totalStudent +1;
                         row.bookedStudent = bookedStudent+","+sname;
                         System.out.println("BOOKING CHANGED");
                         System.out.println("Your Booking ID is--"+bookingId);
                         bmdl.updateBookingHistory(bookingId, sname, row.session, row.exercise, row.week, lId);
                       }
                       else{
                           System.err.println("No space available in current session.");
                       }
                   }
                   else
                   {
                       System.err.println("You are already registed with same session.");
                   }
                   
           }
        }
        
    }
   
    public void initialTimetable(){
        defineTimetable(1,"Saturday","Morning","Yoga","Week1",0,"","£5.00");
        defineTimetable(2,"Saturday","Afternoon","Zumba","Week1",0,"","£7.50");
        defineTimetable(3,"Saturday","Evening","Boxfit","Week1",0,"","£8.50");
        defineTimetable(4,"Sunday","Morning","Aquacise","Week1",0,"","£4.50");
        defineTimetable(5,"Sunday","Afternoon","Yoga","Week1",0,"","£5.00");
        defineTimetable(6,"Sunday","Evening","Zumba","Week1",0,"","£7.50");
        
        defineTimetable(7,"Saturday","Morning","Yoga","Week2",0,"","£5.00");
        defineTimetable(8,"Saturday","Afternoon","Zumba","Week2",0,"","£7.50");
        defineTimetable(9,"Saturday","Evening","Aquacise","Week2",0,"","£4.50");
        defineTimetable(10,"Sunday","Morning","Aquacise","Week2",0,"","£4.50");
        defineTimetable(11,"Sunday","Afternoon","Zumba","Week2",0,"","£7.50");
        defineTimetable(12,"Sunday","Evening","Yoga","Week2",0,"","£5.00");
        
        defineTimetable(13,"Saturday","Morning","Yoga","Week3",0,"","£5.00");
        defineTimetable(14,"Saturday","Afternoon","Aquacise","Week3",0,"","£4.50");
        defineTimetable(15,"Saturday","Evening","Boxfit","Week3",0,"","£8.50");
        defineTimetable(16,"Sunday","Morning","Yoga","Week3",0,"","£5.00");
        defineTimetable(17,"Sunday","Afternoon","Zumba","Week3",0,"","£7.50");
        defineTimetable(18,"Sunday","Evening","Boxfit","Week3",0,"","£8.50");
        
        defineTimetable(19,"Saturday","Morning","Aquacise","Week4",0,"","£4.50");
        defineTimetable(20,"Saturday","Afternoon","Yoga","Week4",0,"","£5.00");
        defineTimetable(21,"Saturday","Evening","Boxfit","Week4",0,"","£8.50");
        defineTimetable(22,"Sunday","Morning","Yoga","Week4",0,"","£5.00");
        defineTimetable(23,"Sunday","Afternoon","Zumba","Week4",0,"","£7.50");
        defineTimetable(24,"Sunday","Evening","Boxfit","Week4",0,"","£8.50");
        
        defineTimetable(25,"Saturday","Morning","Zumba","Week5",0,"","£7.50");
        defineTimetable(26,"Saturday","Afternoon","Aquacise","Week5",0,"","£4.50");
        defineTimetable(27,"Saturday","Evening","Boxfit","Week5",0,"","£8.50");
        defineTimetable(28,"Sunday","Morning","Yoga","Week5",0,"","£5.00");
        defineTimetable(29,"Sunday","Afternoon","Zumba","Week5",0,"","£7.50");
        defineTimetable(30,"Sunday","Evening","Aquacise","Week5",0,"","£4.50");
        
        defineTimetable(31,"Saturday","Morning","Aquacise","Week6",0,"","£4.50");
        defineTimetable(32,"Saturday","Afternoon","Zumba","Week6",0,"","£7.50");
        defineTimetable(33,"Saturday","Evening","Yoga","Week6",0,"","£5.00");
        defineTimetable(34,"Sunday","Morning","Yoga","Week6",0,"","£5.00");
        defineTimetable(35,"Sunday","Afternoon","Zumba","Week6",0,"","£7.50");
        defineTimetable(36,"Sunday","Evening","Boxfit","Week6",0,"","£8.50");
        
        defineTimetable(37,"Saturday","Morning","Zumba","Week7",0,"","£7.50");
        defineTimetable(38,"Saturday","Afternoon","Aquacise","Week7",0,"","£4.50");
        defineTimetable(39,"Saturday","Evening","Boxfit","Week7",0,"","£8.50");
        defineTimetable(40,"Sunday","Morning","Yoga","Week7",0,"","£5.00");
        defineTimetable(41,"Sunday","Afternoon","Zumba","Week7",0,"","£7.50");
        defineTimetable(42,"Sunday","Evening","Aquacise","Week7",0,"","£4.50");
        
        defineTimetable(43,"Saturday","Morning","Zumba","Week8",0,"","£7.50");
        defineTimetable(44,"Saturday","Afternoon","Yoga","Week8",0,"","£5.00");
        defineTimetable(45,"Saturday","Evening","Boxfit","Week8",0,"","£8.50");
        defineTimetable(46,"Sunday","Morning","Yoga","Week8",0,"","£5.00");
        defineTimetable(47,"Sunday","Afternoon","Zumba","Week8",0,"","£7.50");
        defineTimetable(48,"Sunday","Evening","Boxfit","Week8",0,"","£8.50");
        
    }
}
