/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author hp
 */
public class BookingHistory {
    public String bID;
    public String sname;
    public String sessionname;
    public String exname;
    public String week;
    public String bdate;
    public int lId;
    
    public BookingHistory(String bId, String sname, String sessionname, String exname, String week, int lId){
        this.bID = bId;
        this.sname = sname;
        this.sessionname = sessionname;
        this.week = week;
        this.exname = exname;
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDateTime now = LocalDateTime.now();
        this.bdate = dtf.format(now);
        this.lId = lId;
    }
    
    @Override
    public String toString(){
        return this.bID+" | "+this.sname+" | "+this.sessionname+" | "+this.exname+" | "+this.week+" | "+this.bdate;
    }
}
