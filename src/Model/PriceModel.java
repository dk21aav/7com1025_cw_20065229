/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;
import POJO.Price;
import java.util.ArrayList;
/**
 *
 * @author hp
 */
public class PriceModel {
    
    ArrayList<Price> monthlychampionrpt = new ArrayList();
    Price p = new Price();
    
    boolean isLsnChampRpt = false;
    public boolean addLsnChampRpt(String exname, double income){
        monthlychampionrpt.add(new Price(exname, income));
        isLsnChampRpt = true;
        return isLsnChampRpt;
    }
    
    public void reportData(double yogasum, double zumbasum, double aquacisesum, double boxfitsum){
          addLsnChampRpt("Yoga         ",yogasum);
          addLsnChampRpt("Zumba        ",zumbasum);
          addLsnChampRpt("Aquacise     ",aquacisesum);
          addLsnChampRpt("Boxfit       ",boxfitsum);
    }
    public void calculateIncome(String exname){
        switch(exname){
            case "Yoga":
                p.sumyogaPrice = p.sumyogaPrice + p.yogaPrice;
                break;
            case "Zumba":
                p.sumzumbaPrice = p.sumzumbaPrice + p.zumbaPrice;
                break;
            case "Aquacise":
                p.sumacquaPrice = p.sumacquaPrice + p.acquaPrice;
                break;
            case "Boxfit":
                p.sumboxfitPrice = p.sumboxfitPrice + p.boxfitPrice;
                break;
            default:
                break;
        }
    }
    String monthName;
    public void monthlyChampionExRpt(int month){
        switch(month){
            case 1:
                monthName = "JANUARY";
                break;
            case 2:
                monthName = "FEBRUARY";
                break;
            case 3:
                monthName = "MARCH";
                break;
            case 4:
                monthName = "APRIL";
                break;
            case 5:
                monthName = "MAY";
                break;
            case 6:
                monthName = "JUNE";
                break;
            case 7:
                monthName = "JULY";
                break;
            case 8:
                monthName = "AUGUST";
                break;
            case 9:
                monthName = "SEPTEMBER";
                break;
            case 10:
                monthName = "OCTOBER";
                break;
            case 11:
                monthName = "NOVEMBER";
                break;
            case 12:
                monthName = "DECEMBER";
                break;
                
            
        }
        System.out.println("*********MONTHLY CHAMPION EXERCISE REPORT*********");
        System.out.println("Month-"+monthName);
        reportData(p.sumyogaPrice, p.sumzumbaPrice, p.sumacquaPrice, p.sumboxfitPrice);
        monthlychampionrpt.forEach((s) -> {
            System.out.println(s);
        });
        double max = Math.max(Math.max(p.sumacquaPrice,p.sumboxfitPrice),Math.max(p.sumyogaPrice,p.sumzumbaPrice));
        System.out.println("Highest monthly income:"+ max);
    }
}
