/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RunMe;

import Model.BookingHistoryModel;
import Model.LessonModel;
import Model.PriceModel;
import Model.RatingModel;
import Model.StudentModel;
import Model.TimetableModel;
import java.util.Scanner;


/**
 *
 * @author hp
 */
public class RunMe {
    public static void main(String[] args){
        StudentModel smdl = new StudentModel();
        smdl.initialStudents();
        TimetableModel tmdl = new TimetableModel();
        tmdl.initialTimetable();
        RatingModel rmdl = new RatingModel();
        rmdl.initialRatings();
        BookingHistoryModel bmdl = new BookingHistoryModel();
        bmdl.initialBookings();
        LessonModel lmdl = new LessonModel();
        PriceModel pmdl = new PriceModel();
        Scanner input = new Scanner(System.in);
        int choice;
        int checktimetable;
        int day;
        int exercise;
        String sname;
        while(true){
            System.out.println("**************WELCOME TO BOOKING SYSTEM**************");
            System.out.println("What do you want to do?");
            System.out.println("1. Book a group exercise lesson");
            System.out.println("2. Change a booking");
            System.out.println("3. Attend a lesson and rate exercise");
            System.out.println("4. Monthly lesson report");
            System.out.println("5. Monthly champion exercise report");
            System.out.println("6. List all students");
            System.out.println("7. Add students");
            System.out.println("8. List all Bookings");
            System.out.println("9. Exit");
            
            choice = input.nextInt();
        
        
            switch(choice){
                case 1:
                    System.out.println("*****STUDENT DETAILS*****");
                    smdl.listAllStudents();
                    System.out.println("Enter your registed name--");
                    sname = input.next();
                    boolean sReg;
                    sReg = smdl.chkRegStudent(sname);
                    if(!sReg){
                        System.err.println("You are not registered.");
                        break;
                    }
                    else
                    {
                        System.out.println("Select the way to check the timetable:");
                        System.out.println("1. By Day");
                        System.out.println("2. By Exercise");
                        System.out.println("3. Exit");
                        checktimetable = input.nextInt();
                        switch(checktimetable){
                            case 1:
                                System.out.println("Select a day:");
                                System.out.println("1. Saturday");
                                System.out.println("2. Sunday");
                                System.out.println("3. Exit");
                                day = input.nextInt();
                                String dayname;
                                  int lId;
                                switch(day){
                                    case 1:
                                        dayname = "Saturday";
                                        tmdl.timetableByDay(dayname);
                                        System.out.println("Select lesson id to book a lesson:");
                                        lId = input.nextInt();
                                        tmdl.bookLesson(lId,sname);
                                        break;
                                    case 2:
                                        dayname = "Sunday";
                                        tmdl.timetableByDay(dayname);
                                        System.out.println("Select lesson id to book a lesson:");
                                        lId = input.nextInt();
                                        tmdl.bookLesson(lId,sname);
                                        break;
                                    case 3:
                                        break;
                                    default:
                                        System.err.print("Invalid Output");
                                        break;
                                }

                                break;

                            case 2:
                                System.out.println("Select an exercise");
                                System.out.println("1. Yoga");
                                System.out.println("2. Zumba");
                                System.out.println("3. Aquacise");
                                System.out.println("4. BoxFit");
                                System.out.println("5.Exit");
                                exercise = input.nextInt();
                                String exname;
                                switch(exercise){
                                    case 1:
                                        exname = "Yoga";
                                        tmdl.timetableByExName(exname);
                                        System.out.println("Select lesson id to book a lesson:");
                                        lId = input.nextInt();
                                        tmdl.bookLesson(lId,sname);
                                        break;
                                    case 2:
                                        exname = "Zumba";
                                        tmdl.timetableByExName(exname);
//                                        System.out.println("Select a row number for which you want to book lesson for Zumba");
//                                        exrowno = input.nextInt();
//                                        lmdl.bookLessonbyExName(exrowno, exname, sname);
                                        System.out.println("Select lesson id to book a lesson:");
                                        lId = input.nextInt();
                                        tmdl.bookLesson(lId,sname);
                                        break;
                                    case 3:
                                        exname = "Aquacise";
                                        tmdl.timetableByExName(exname);
                                        System.out.println("Select lesson id to book a lesson:");
                                        lId = input.nextInt();
                                        tmdl.bookLesson(lId,sname);
                                        break;
                                    case 4:
                                        exname = "Boxfit";
                                        tmdl.timetableByExName(exname);
                                        System.out.println("Select lesson id to book a lesson:");
                                        lId = input.nextInt();
                                        tmdl.bookLesson(lId,sname);
                                        break;
                                    case 5:
                                        break;
                                    default:
                                        System.err.print("Invalid Output");
                                        break;
                                }
                                break;
                            case 3:
                                break;
                            default:
                                System.err.print("Invalid Output");
                                break;
                        }
                    }
                    break;
                case 2:
                    String bId;
                    String modifyBooking;
                    System.out.println("Enter yout booking ID:");
                    bId = input.next();
                    System.out.println("What you want to do?");
                    System.out.println("1. Cancel booking");
                    System.out.println("2. Change booking");
                    System.out.println("3. Exit");
                    modifyBooking = input.next();
                    
                    switch(modifyBooking){
                        case "1":
                            tmdl.cancelBooking(bId);
                            break;
                        case "2":
                            tmdl.changeBooking(bId);
                            break;
                        case "3":
                            break;
                        default:
                            System.err.print("Invalid Output");
                            break;
                    }
                    
                    break;
                    
                case 3:
                    String lsnAtt;
                    int star;
                    String value = null;
                    System.out.println("Have you attended a lesson?(Y/N)");
                    lsnAtt = input.next();
                    switch(lsnAtt){
                        case "Y":
                            System.out.println("Please rate the attended lesson");
                            System.out.println("1. Very dissatisfied");
                            System.out.println("2. Dissatisfied");
                            System.out.println("3. Ok");
                            System.out.println("4. Satisfied");
                            System.out.println("5. Very Satisfied");
                            star = input.nextInt();
                            switch(star){
                                case 1:
                                    value = "Very dissatisfied";
                                    break;
                                case 2:
                                    value= "Dissatisfied";
                                    break;
                                case 3:
                                    value= "Ok";
                                    break;
                                case 4:
                                    value= "Satisfied";
                                    break;
                                case 5:
                                    value= "Very Satisfied";
                                    break;
                                default:
                                    System.err.print("Invalid Output");
                                    break;
                            }
                            rmdl.addRating(star, value);
                            System.out.println("Thank you for your feedback.");
                            break;
                        case "N":
                            break;
                    }
                    break;
                
                case 4:
                    int monthNo;
                    System.out.println("Enter month to view monthly lesson report-");
                    monthNo = input.nextInt();
                    lmdl.getRecordforRpt(monthNo);
                    System.out.println("*************End of Report*************");
                    break;
                case 5:
                    int monthNum;
                    System.out.println("Enter month to view monthly champion exercise report-");
                    monthNum = input.nextInt();
                    tmdl.monthlyChampionRpt(monthNum);
                    System.out.println("*****************End of Report*****************");
                    break;
                case 6:
                    smdl.listAllStudents();
                    break;
                
                case 7:
                    System.out.println("Enter your student ID:");
                    int id = input.nextInt();
                    System.out.println("Enter your name:");
                    String name = input.next();
                    smdl.addStudent(id, name);
                    System.out.println("Student Registered.");
                    break;
                    
                case 8:
                    System.out.println("****************BOOKING DETAILS****************");
                    bmdl.listAllBookings();
                    tmdl.listBooking();
                    break;
                    
                case 9:
                    break;
                    
                default:
                    System.err.print("Invalid Output");
                    break;
                
            }
        }
    }
}
