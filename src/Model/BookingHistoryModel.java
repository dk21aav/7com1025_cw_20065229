/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import POJO.BookingHistory;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 *
 * @author hp
 */
public class BookingHistoryModel {
    ArrayList<BookingHistory> bh = new ArrayList();
    
    public void listAllBookings(){
        bh.forEach((s) -> {
            System.out.println(s);
        });
    }
    
    public void addBookingHistory(String bId, String sname, String sessionname, String exname, String week, int lId){
        bh.add(new BookingHistory(bId, sname, sessionname, exname, week, lId));
    }
//    public void defineBookings(String bId, String sname, String sessionname, String exname, String week, int lId){
//        bh.add(new BookingHistory(bId, sname, sessionname, exname, week, lId));
//    }
    
    public void initialBookings(){
        addBookingHistory("BKN100","Dhruvi","Morning","Yoga","Week1",33);
        addBookingHistory("BKN101","Dhrumil","Morning","Yoga","Week1",33);
        addBookingHistory("BKN102","Bhumil","Morning","Yoga","Week1",33);
        addBookingHistory("BKN103","Tanuja","Morning","Yoga","Week1",33);
        addBookingHistory("BKN104","Nimi","Morning","Yoga","Week1",33);
        addBookingHistory("BKN105","Dhruvi","Morning","Yoga","Week1",33);
        addBookingHistory("BKN106","Joy","Morning","Yoga","Week1",33);
        addBookingHistory("BKN107","Dhruvi","Morning","Yoga","Week1",33);
        addBookingHistory("BKN108","Jigar","Morning","Yoga","Week1",33);
        addBookingHistory("BKN109","Joy","Morning","Yoga","Week1",33);
        addBookingHistory("BKN110","Dhruvi","Morning","Yoga","Week1",33);
        addBookingHistory("BKN111","Krinal","Morning","Yoga","Week1",33);
        addBookingHistory("BKN112","Dhruvi","Morning","Yoga","Week1",33);
        addBookingHistory("BKN113","Hari","Morning","Yoga","Week1",33);
        addBookingHistory("BKN114","Dhruvi","Morning","Yoga","Week1",33);
        addBookingHistory("BKN115","Bhumil","Morning","Yoga","Week1",33);
        addBookingHistory("BKN116","Tanuja","Morning","Yoga","Week1",33);
        addBookingHistory("BKN117","Dhrumil","Morning","Yoga","Week1",33);
        addBookingHistory("BKN118","Dhruvi","Morning","Yoga","Week1",33);
        addBookingHistory("BKN119","Krinal","Morning","Yoga","Week1",33);

    }
    String finalval;
    public String cancelBooking(String bId){
        int lId =0;
        String sname;
        for(int i=0; i< bh.size();i++){
            if(bh.get(i).bID.equals(bId)){
                lId = bh.get(i).lId;
                sname = bh.get(i).sname;
                finalval = Integer.toString(lId)+","+sname;
                bh.remove(i);
            }
        }
        return finalval;
    }
    String stuname;
    public String getStudentName(String bId){
        for(int i=0; i< bh.size();i++){
            if(bh.get(i).bID.equals(bId)){
                stuname = bh.get(i).sname;
            }
        }
        return stuname;
    }
    int oldLid;
    public int getOldLid(String bId){
        for(int i=0; i< bh.size();i++){
            if(bh.get(i).bID.equals(bId)){
                oldLid = bh.get(i).lId;
            }
        }
        return oldLid;
    }
    public void updateBookingHistory(String bookingId,String sname,String session, String exercise, String week, int lId){
        for(int i=0; i< bh.size();i++){
            if(bh.get(i).bID.equals(bookingId)){
                bh.get(i).sname = sname;
                bh.get(i).sessionname = session;
                bh.get(i).exname = exercise;
                bh.get(i).week = week;
                bh.get(i).lId = lId;
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                LocalDateTime now = LocalDateTime.now();
                bh.get(i).bdate = dtf.format(now);
            }
        }
    }
}
