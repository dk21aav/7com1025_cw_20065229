/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;

/**
 *
 * @author hp
 */
public class Timetable {
    public final int id;
    public final String day;
    public final String session;
    public final String exercise;
    public final String week;
    public int count;
    public String bookedStudent;
    public String price;
    
    public Timetable(int id, String day, String session, String exercise, String week, int count, String bookedStudent, String price){
        this.day = day;
        this.session = session;
        this.exercise = exercise;
        this.week = week;
        this.id = id;
        this.count = count;
        this.bookedStudent = bookedStudent;
        this.price = price;
    }
    @Override
    public String toString(){
        return this.id+" | "+this.day+" | "+this.session+" | "+this.exercise+" | "+this.week+" | "+this.price;
    }
}
