/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;

/**
 *
 * @author hp
 */
public class Price {
    public double yogaPrice = 5;
    public double zumbaPrice = 7.5;
    public double acquaPrice = 4.5;
    public double boxfitPrice = 8.5;
    public double sumyogaPrice = 5;
    public double sumzumbaPrice = 7.5;
    public double sumacquaPrice = 4.5;
    public double sumboxfitPrice = 8.5;
    public String exname;
    public double income;
    
    public Price(String exname, double income){
        this.exname = exname;
        this.income = income;
    }
    public Price(){
    }
    
    @Override
    public String toString(){
        return this.exname+" | "+this.income;
    }
}
