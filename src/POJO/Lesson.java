/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;


/**
 *
 * @author hp
 */
public class Lesson {
    public String exname;
    public String avgrate;
    
    public Lesson(String exname, String avgrate){
        this.exname = exname;
        this.avgrate = avgrate;
    }
    
    @Override
    public String toString(){
        return this.exname+" | "+this.avgrate;
    }
}
